package testall;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class TestAll {

    private static File file = new File ( "/Users/nyucute/Documents/mobilges.xml" );
    
    public static void main(String[] args) {

        TestAll test = new TestAll();
        
        Configuracion configuracion = new Configuracion();
        Empresa empresa = new Empresa();
        empresa.setId("E01");
        empresa.setWebservice("localhost:8080");        
        empresa.setNombreformato( "ConsolidadoFacturas" );
        empresa.setRutaformato("/Users/nyucute/Documents/");
        empresa.setCopias(1);        
        configuracion.getEmpresa().add( empresa );
        
        empresa = new Empresa();
        empresa.setId("E07");
        empresa.setWebservice("localhost");        
        empresa.setNombreformato( "ConsolidadoFacturasDimisa" );
        empresa.setRutaformato("/Users/nyucute/Documents/");
        empresa.setCopias(1);
        configuracion.getEmpresa().add( empresa );

        
        if( grabarConfiguracion( configuracion ) ){
            System.out.println(" Configuracion "+configuracion);
        }
        
        System.out.println("");
        configuracion = leerConfiguracion();
        
        for( Empresa emp : configuracion.getEmpresa() ){
            if ( empresa.getId().equals("E01") ){
                System.out.println("Empresa "+ emp.getId() );
                System.out.println("WS "+emp.getWebservice());
                System.out.println("Ruta "+emp.getRutaformato());
                System.out.println("Formato "+emp.getNombreformato());
                System.out.println("Copias "+emp.getCopias());
            }
        }
        
    }
    
        public void crearFichero( String urlArchivoGFace, String pFactura ){
        
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter( urlArchivoGFace + "FACE_NPG_MOBILGES.txt");
            System.err.println("************************************************ "+fichero.toString() );
            pw = new PrintWriter(fichero);
 
                pw.println( pFactura );
 
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        }
        
    public static Configuracion leerConfiguracion (){
        Configuracion configuracion = new Configuracion();
        try {
            
            JAXBContext jaxbContext = JAXBContext.newInstance( Configuracion.class );
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            configuracion = (Configuracion) jaxbUnmarshaller.unmarshal( file );
                        
        } catch (JAXBException ex) {
            Logger.getLogger(TestAll.class.getName()).log(Level.SEVERE, null, ex);
        }
        return configuracion;
    }
    
    public static boolean grabarConfiguracion( Configuracion configuracion ){

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance( Configuracion.class );
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            
            jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT , true );
            
            jaxbMarshaller.marshal( configuracion , file );
            jaxbMarshaller.marshal( configuracion , System.out );

            return true;
        } catch (JAXBException ex) {
            Logger.getLogger(TestAll.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }    

    public static void leerFichero (){
        String ruta = "/Users/nyucute/Documents/archivo.txt";
        File archivo = new File(ruta);
        BufferedWriter bw = null;
        try {
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write("El fichero de texto ya estaba creado.");
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write("Acabo de crear el fichero de texto.");
            }
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(TestAll.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
