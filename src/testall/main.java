/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testall;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author nyucute
 */
public class main {

    public static void main(String[] args) {

        String pInicial = "100"; 
        String pFinal = "200000";
        
        pInicial = armarNumero( pInicial , pFinal.length() );
        System.out.println("Inicial "+pInicial);
        System.out.println("Final   "+pFinal);
        
    }

    public static String armarNumero( String cadenaE, int cantidadCaracteres ){
        
        if ( cadenaE.length() == cantidadCaracteres ){
            return cadenaE;
        }
        String nuevaCadena = new String();
        int limite = cantidadCaracteres - cadenaE.length();
        for ( int i = 0 ; i < limite ; i++ ){
            nuevaCadena = "0"+nuevaCadena;
        }
        nuevaCadena = nuevaCadena+cadenaE;
        return nuevaCadena;
    }        
    
    /**
     * 
     * @param archivoXml Un String con el formato xml
     * @param tag El nombre del tag del que se desea tener la información 
     * @param tipo Si se requiere el valor o el atributo del tag ejem 0 = Valor ,  1 =  atributo 
     * @param atributo El nombre del atributo que se desea obtener, es valido unicamente si tipo = 1
     * @return 
     */
    public static String obtenerValorXML( String archivoXml, String tag, Integer tipo, String atributo ) {
        String respuesta = "error";
        try {
            SAXBuilder builder = new SAXBuilder();
            InputStream stream = new ByteArrayInputStream(archivoXml.getBytes("UTF-8"));
            Document document = (Document) builder.build(stream);
            Element rootNode = document.getRootElement();
            List<Element> elementos = rootNode.getChildren();
            for (Element element : elementos) {
                if (element.getName().equals( tag ) ) {
                    if ( tipo ==0 ){
                        respuesta = element.getValue();
                    }else{
                        respuesta = element.getAttributeValue( atributo );
                    }
                }
            }
        } catch (IOException | JDOMException io) {
            respuesta = "error";
        }
        return respuesta;
    }

    /**
     * 
     * @param archivoXml Un String con el formato xml
     * @param tagPadre El nombre del tag que contiene el conjunto de tag hijos
     * @param tagHijo El nombre del tag del que se desea tener la información
     * @param tipo Si se requiere el valor o el atributo del tag ejem 0 = Valor ,  1 =  atributo 
     * @param atributo El nombre del atributo que se desea obtener, es valido unicamente si tipo = 1
     * @return 
     */
    public static String obtenerValorXML(String archivoXml, String tagPadre, String tagHijo, Integer tipo, String atributo ) {
        String respuesta = "error";
        try {
            SAXBuilder builder = new SAXBuilder();
            InputStream stream = new ByteArrayInputStream(archivoXml.getBytes("UTF-8"));
            Document document = (Document) builder.build(stream);
            Element rootNode = document.getRootElement();
            List<Element> elementos = rootNode.getChildren();
            List<Element> elementosHijo = new ArrayList();

            for (Element element : elementos) {
                if (element.getName().equals(tagPadre)) {
                    elementosHijo = element.getChildren();
                }
            }

            for (Element element : elementosHijo) {
                if (element.getName().equals(tagHijo)) {
                    if ( tipo == 0 ){
                        respuesta = element.getValue();
                    }else if ( tipo ==1 ) {
                        respuesta = element.getAttributeValue( atributo );
                    }
                }
            }
            
            

        } catch (IOException | JDOMException io) {
            respuesta = "error";
        }

        return respuesta;
    }

    /**
     * 
     * @param archivoXml Un String con el formato xml
     * @param tagList Contiene un arreglo de tag el cual debe de venir en orden desde el primer tag hasta el que se desea trabajar para obtener el valor o atributo
     * @param tipo Si se requiere el valor o el atributo del tag ejem 0 = Valor ,  1 =  atributo 
     * @param atributo El nombre del atributo que se desea obtener, es valido unicamente si tipo = 1
     * @return 
     */
    public static String obtenerValorXML( String archivoXml, List<String> tagList, Integer tipo, String atributo ) {
        String respuesta = "error";
        try {
            SAXBuilder builder = new SAXBuilder();
            InputStream stream = new ByteArrayInputStream(archivoXml.getBytes("UTF-8"));
            Document document = (Document) builder.build(stream);
            Element rootNode = document.getRootElement();
            
            int contador = 0;
            for ( String tag : tagList ){
                contador ++ ;
                rootNode = rootNode.getChild( tag );
                if ( contador == tagList.size() ){
                    if ( tipo == 0 ){
                        respuesta = rootNode.getValue();
                    }else if ( tipo ==1 ) {
                        respuesta = rootNode.getAttributeValue( atributo );
                    }
                    break;
                }

            }
            

        } catch (IOException | JDOMException io) {
            respuesta = "error";
        }

        return respuesta;
    }

    /**
     * 
     * @param archivoXml Un String con el formato xml
     * @param tagList Contiene un arreglo de tag el cual debe de venir en orden desde el primer tag hasta el que se desea trabajar para obtener el valor o atributo
     * @param tipo Si se requiere el valor o el atributo del tag,  0 = Valor ,  1 =  atributo 
     * @param atributo El nombre del atributo que se desea obtener, es valido unicamente si tipo = 1 de lo contrario enviar null
     * @return 
     */
    public static String obtenerValorXML2( String archivoXml, List<String> tagList, Integer tipo, String atributo ) {
        String respuesta = "error";
        try {
            SAXBuilder builder = new SAXBuilder();
            InputStream stream = new ByteArrayInputStream(archivoXml.getBytes("UTF-8"));
            Document document = (Document) builder.build(stream);
            Element rootNode = document.getRootElement();
            List<Element> elementos = rootNode.getChildren();
            
            int contador = 0;
            for ( String tag : tagList ){
                contador ++ ;
                for ( Element ele : elementos ){
                    if ( ele.getName().equals( tag ) ){
                        if ( contador == tagList.size() ){                            
                            if ( tipo == 0 ){
                                respuesta = ele.getValue();
                            }else if ( tipo ==1 ) {
                                respuesta = ele.getAttributeValue( atributo );
                            }
                            break;                            
                        }else{
                            elementos = ele.getChildren();
                        break;
                        }
                    }                
                }

            }
            

        } catch (IOException | JDOMException io) {
            respuesta = "error";
        }

        return respuesta;
    }        
    
    public static String obtenerString() {
        String texto = "<?xml version=\"1.0\"?>\n"
                + "<ProcessResult>\n"
                + "    <Process>\n"
                + "        <Name>Signature.xDoc.Web.Services</Name>\n"
                + "        <Type>6</Type>\n"
                + "        <TypeDesc>appWebServices</TypeDesc>\n"
                + "    </Process>\n"
                + "    <HasError>false</HasError>\n"
                + "    <Error>\n"
                + "        <Code />\n"
                + "        <Description />\n"
                + "        <UIMessage />\n"
                + "    </Error>\n"
                + "    <ReturnValue>0</ReturnValue>\n"
                + "    <TextData>&lt;?xml version=\"1.0\" encoding=\"utf-8\"?&gt;\n"
                + "&lt;DTE xmlns:dte=\"http://se.sat.gob.gt/aplicaciones/DTE\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:gs1=\"urn:ean.ucc:pay:2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"&gt;\n"
                + "  &lt;Documento Id=\"FACE44252\"&gt;\n"
                + "    &lt;gs1:invoice creationDateTime=\"2016-03-15T00:00:00\" documentStatus=\"ORIGINAL\" xmlns:gs1=\"urn:ean.ucc:pay:2\"&gt;\n"
                + "      &lt;contentVersion&gt;\n"
                + "        &lt;versionIdentification&gt;1.0&lt;/versionIdentification&gt;\n"
                + "      &lt;/contentVersion&gt;\n"
                + "      &lt;documentStructureVersion&gt;\n"
                + "        &lt;versionIdentification&gt;Version 1.0&lt;/versionIdentification&gt;\n"
                + "      &lt;/documentStructureVersion&gt;\n"
                + "      &lt;invoiceIdentification&gt;\n"
                + "        &lt;uniqueCreatorIdentification&gt;FACE|201356621072514|FACE-66-AA-001|160000044252&lt;/uniqueCreatorIdentification&gt;\n"
                + "        &lt;contentOwner&gt;\n"
                + "          &lt;gln&gt;0000000000000&lt;/gln&gt;\n"
                + "        &lt;/contentOwner&gt;\n"
                + "      &lt;/invoiceIdentification&gt;\n"
                + "      &lt;invoiceCurrency&gt;\n"
                + "        &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n"
                + "      &lt;/invoiceCurrency&gt;\n"
                + "      &lt;invoiceType&gt;INVOICE&lt;/invoiceType&gt;\n"
                + "      &lt;countryOfSupplyOfGoods&gt;\n"
                + "        &lt;countryISOCode&gt;GT&lt;/countryISOCode&gt;\n"
                + "      &lt;/countryOfSupplyOfGoods&gt;\n"
                + "      &lt;buyer&gt;\n"
                + "        &lt;partyIdentification&gt;\n"
                + "          &lt;gln&gt;0000000000000&lt;/gln&gt;\n"
                + "        &lt;/partyIdentification&gt;\n"
                + "        &lt;nameAndAddress&gt;\n"
                + "          &lt;city&gt;MIXCO&lt;/city&gt;\n"
                + "          &lt;countryCode&gt;\n"
                + "            &lt;countryISOCode&gt;0&lt;/countryISOCode&gt;\n"
                + "          &lt;/countryCode&gt;\n"
                + "          &lt;languageOfTheParty&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/languageOfTheParty&gt;\n"
                + "          &lt;name&gt;LUCRECIA MEDINA&lt;/name&gt;\n"
                + "          &lt;state&gt;GUATEMALA&lt;/state&gt;\n"
                + "          &lt;streetAddressOne&gt;Res Los Fresnos Casa 33 Zona 8, San Cristobal&lt;/streetAddressOne&gt;\n"
                + "        &lt;/nameAndAddress&gt;\n"
                + "        &lt;companyRegistrationNumber&gt;1972704-6&lt;/companyRegistrationNumber&gt;\n"
                + "      &lt;/buyer&gt;\n"
                + "      &lt;seller&gt;\n"
                + "        &lt;partyIdentification&gt;\n"
                + "          &lt;gln&gt;0000000000000&lt;/gln&gt;\n"
                + "          &lt;additionalPartyIdentification&gt;\n"
                + "            &lt;additionalPartyIdentificationValue&gt;COMPANIA DISTRIBUIDORA MIREDA, S.A.&lt;/additionalPartyIdentificationValue&gt;\n"
                + "            &lt;additionalPartyIdentificationType&gt;FOR_INTERNAL_USE_1&lt;/additionalPartyIdentificationType&gt;\n"
                + "          &lt;/additionalPartyIdentification&gt;\n"
                + "          &lt;additionalPartyIdentification&gt;\n"
                + "            &lt;additionalPartyIdentificationValue&gt;9999&lt;/additionalPartyIdentificationValue&gt;\n"
                + "            &lt;additionalPartyIdentificationType&gt;FOR_INTERNAL_USE_2&lt;/additionalPartyIdentificationType&gt;\n"
                + "          &lt;/additionalPartyIdentification&gt;\n"
                + "        &lt;/partyIdentification&gt;\n"
                + "        &lt;nameAndAddress&gt;\n"
                + "          &lt;city&gt;GUATEMALA&lt;/city&gt;\n"
                + "          &lt;countryCode&gt;\n"
                + "            &lt;countryISOCode&gt;GT&lt;/countryISOCode&gt;\n"
                + "          &lt;/countryCode&gt;\n"
                + "          &lt;languageOfTheParty&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/languageOfTheParty&gt;\n"
                + "          &lt;name&gt;COMPANIA DISTRIBUIDORA MIREDA, S.A.&lt;/name&gt;\n"
                + "          &lt;state&gt;GUATEMALA&lt;/state&gt;\n"
                + "          &lt;streetAddressOne&gt;Avenida Ferrocarril 19-97 Zona 12 Empresarial El Cortijo int 610&lt;/streetAddressOne&gt;\n"
                + "        &lt;/nameAndAddress&gt;\n"
                + "        &lt;companyRegistrationNumber&gt;5835682&lt;/companyRegistrationNumber&gt;\n"
                + "      &lt;/seller&gt;\n"
                + "      &lt;invoiceLineItem number=\"1\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;1.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;52.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 8.12 RUBIO CLARO CENIZO IRISADO&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;46.43&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;46.43&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;5.57&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"2\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;2.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;104.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 6.0 RUBIO OSCURO NAT&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;92.86&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;92.86&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;11.14&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"3\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;2.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;104.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 7.0 RUBIO MED NATURAL&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;92.86&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;92.86&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;11.14&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"4\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;1.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;52.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 5.4 CASTANO CLARO COBRIZO&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;46.43&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;46.43&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;5.57&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"5\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;1.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;52.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 9.21 BEIGE CENIZO IRIS&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;46.43&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;46.43&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;5.57&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"6\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;2.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;104.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 8.7 RUBIO CLARO MATE&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;92.86&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;92.86&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;11.14&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"7\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;2.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;104.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 8.31 RUBIO CLARO DORADO CENIZO&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;92.86&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;92.86&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;11.14&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"8\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;1.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;52.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;TECHNI-TONE 5.5 CASTANO CLARO CAOBA&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;46.43&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;52.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;46.43&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;46.43&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;5.57&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"9\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;1.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;115.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;Combo Crema reveladora 1000ml E-M 11&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;102.68&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;115.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;102.68&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;102.68&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;12.32&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"10\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;2.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;0.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;CREME DEVELOPER 9% 30 VOL 1000 ML.&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;41.96&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;47.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;0.00&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;0.00&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;0.00&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceLineItem number=\"11\"&gt;\n"
                + "        &lt;invoicedQuantity&gt;\n"
                + "          &lt;value&gt;1.00&lt;/value&gt;\n"
                + "          &lt;unitOfMeasure&gt;\n"
                + "            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n"
                + "          &lt;/unitOfMeasure&gt;\n"
                + "        &lt;/invoicedQuantity&gt;\n"
                + "        &lt;transferOfOwnershipDate&gt;2016-03-15&lt;/transferOfOwnershipDate&gt;\n"
                + "        &lt;amountInclusiveAllowancesCharges&gt;0.00&lt;/amountInclusiveAllowancesCharges&gt;\n"
                + "        &lt;itemDescription&gt;\n"
                + "          &lt;language&gt;\n"
                + "            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n"
                + "          &lt;/language&gt;\n"
                + "          &lt;text&gt;DEVELOPER 20 VOL 1000 ML.&lt;/text&gt;\n"
                + "        &lt;/itemDescription&gt;\n"
                + "        &lt;itemPriceBaseQuantity&gt;\n"
                + "          &lt;value&gt;41.96&lt;/value&gt;\n"
                + "        &lt;/itemPriceBaseQuantity&gt;\n"
                + "        &lt;itemPriceInclusiveAllowancesCharges&gt;47.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n"
                + "        &lt;invoiceLineItemInformationAfterTaxes&gt;\n"
                + "          &lt;amountExclusiveAllowancesCharges&gt;0.00&lt;/amountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n"
                + "        &lt;invoiceLineTaxInformation&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;0.00&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;0.00&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/invoiceLineTaxInformation&gt;\n"
                + "      &lt;/invoiceLineItem&gt;\n"
                + "      &lt;invoiceTotals&gt;\n"
                + "        &lt;totalInvoiceAmount&gt;381.25&lt;/totalInvoiceAmount&gt;\n"
                + "        &lt;totalAmountInvoiceAllowancesCharges&gt;0.00&lt;/totalAmountInvoiceAllowancesCharges&gt;\n"
                + "        &lt;totalLineAmountExclusiveAllowancesCharges&gt;381.25&lt;/totalLineAmountExclusiveAllowancesCharges&gt;\n"
                + "        &lt;totalTaxAmount&gt;45.75&lt;/totalTaxAmount&gt;\n"
                + "        &lt;taxSubTotal&gt;\n"
                + "          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n"
                + "          &lt;taxableAmount&gt;381.25&lt;/taxableAmount&gt;\n"
                + "          &lt;taxAmount&gt;45.75&lt;/taxAmount&gt;\n"
                + "          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n"
                + "        &lt;/taxSubTotal&gt;\n"
                + "        &lt;totalInvoiceAmountPayable&gt;381.25&lt;/totalInvoiceAmountPayable&gt;\n"
                + "        &lt;taxAccountingCurrency&gt;\n"
                + "          &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n"
                + "        &lt;/taxAccountingCurrency&gt;\n"
                + "        &lt;baseAmount&gt;427.00&lt;/baseAmount&gt;\n"
                + "      &lt;/invoiceTotals&gt;\n"
                + "      &lt;taxCurrencyInformation&gt;\n"
                + "        &lt;currencyConversionFrom&gt;\n"
                + "          &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n"
                + "        &lt;/currencyConversionFrom&gt;\n"
                + "        &lt;currencyConversionTo&gt;\n"
                + "          &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n"
                + "        &lt;/currencyConversionTo&gt;\n"
                + "        &lt;exchangeRate&gt;1.00&lt;/exchangeRate&gt;\n"
                + "      &lt;/taxCurrencyInformation&gt;\n"
                + "      &lt;extension&gt;\n"
                + "        &lt;fechaResolucion&gt;2013-03-22&lt;/fechaResolucion&gt;\n"
                + "        &lt;serieAutorizada&gt;FACE-66-AA-001&lt;/serieAutorizada&gt;\n"
                + "        &lt;rangoInicialAutorizado&gt;000001&lt;/rangoInicialAutorizado&gt;\n"
                + "        &lt;rangoFinalAutorizado&gt;100000&lt;/rangoFinalAutorizado&gt;\n"
                + "        &lt;informacionRegimenIsr&gt;PAGO_TRIMESTRAL&lt;/informacionRegimenIsr&gt;\n"
                + "      &lt;/extension&gt;\n"
                + "    &lt;/gs1:invoice&gt;\n"
                + "    &lt;CAE&gt;&lt;DCAE Id=\"CAE160000044252\"&gt;&lt;TipoCAE&gt;CAE&lt;/TipoCAE&gt;&lt;NITGFACE&gt;43430775&lt;/NITGFACE&gt;&lt;NITEmisor&gt;5835682&lt;/NITEmisor&gt;&lt;NITReceptor&gt;1972704-6&lt;/NITReceptor&gt;&lt;Serie&gt;FACE-66-AA-001&lt;/Serie&gt;&lt;NumeroDocumento&gt;160000044252&lt;/NumeroDocumento&gt;&lt;FechaEmision&gt;2016-03-15&lt;/FechaEmision&gt;&lt;NumeroAutorizacion&gt;201356621072514&lt;/NumeroAutorizacion&gt;&lt;IngresosNetosGravados&gt;381.25&lt;/IngresosNetosGravados&gt;&lt;IVA&gt;45.75&lt;/IVA&gt;&lt;ImporteTotal&gt;427.00&lt;/ImporteTotal&gt;&lt;/DCAE&gt;&lt;FCAE&gt;&lt;ds:SignedInfo&gt;&lt;ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#dsa-sha1\" /&gt;&lt;ds:Reference URI=\"#CAE160000044252\"&gt;&lt;ds:Transforms&gt;&lt;ds:Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;/ds:Transforms&gt;&lt;ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /&gt;&lt;ds:DigestValue&gt;v7tsYBB6FXlNF/G9JS4R+hLBkI4=&lt;/ds:DigestValue&gt;&lt;/ds:Reference&gt;&lt;/ds:SignedInfo&gt;&lt;ds:SignatureValue&gt;\n"
                + "jDvuo74Zl8Rs+Khbp631SwTm/olk2S0vUyW//SWZruQ3dCifgjLDfQ==\n"
                + "&lt;/ds:SignatureValue&gt;&lt;ds:KeyInfo&gt;&lt;ds:KeyValue&gt;&lt;ds:DSAKeyValue&gt;&lt;ds:P&gt;\n"
                + "xVpzXTyIquFGWTmPxP7NtfqHlCqcmB4tp7/AM1ziOVIXeCcG5dYHZY6jwXfsEY/F\n"
                + "YtgBIa6HoXf+eFQQ638/12XMKJP3bnbX1bvkdunoeb7wVuFpIfR23WWx8lJdWvl+\n"
                + "ajXCkYm8mVfBh+/padzvjB+X8UWOkVaUs1/2xUzFNI0=\n"
                + "&lt;/ds:P&gt;&lt;ds:Q&gt;\n"
                + "2DbdlCGHX4G7QXAHJp933DxAUOM=\n"
                + "&lt;/ds:Q&gt;&lt;ds:G&gt;\n"
                + "TMFId4Zsx9+cQed0pnMOa8be/1i4p7i4RP43BRJutFotMzQEE9udEdaL3wKR0sXT\n"
                + "IzmPClHHXp0kWxXSeHBYU4dozXldeethob+p19GwIzVfRvWg3TJg8XYaTgsqFt6m\n"
                + "ofiRHjcPXAVSDAbGFkUymUbZTBBHHhr6uGpfLHcIaNw=\n"
                + "&lt;/ds:G&gt;&lt;ds:Y&gt;\n"
                + "Kku3IMvaslsCGoK9fIPA1ZVfrDauPPTBYO91Katht93a/3OFifmuiJTir3RzZmeN\n"
                + "kOho2dROM7/yr8iHYs9iyV4yMBIeFFmSSoQkeZsrVf/C8i5Op4fYe4ZAtzbdmY3r\n"
                + "wZJiAtzyn/xLn7l+07Xf5gSG8e4wnitBk9Pjo1g1aC0=\n"
                + "&lt;/ds:Y&gt;&lt;/ds:DSAKeyValue&gt;&lt;/ds:KeyValue&gt;&lt;ds:X509Data&gt;&lt;ds:X509Certificate&gt;\n"
                + "MIIDCTCCAsigAwIBAgIBATAJBgcqhkjOOAQDMEAxFzAVBgNVBAMMDkNlcnRpRmly\n"
                + "bWFzIENBMRgwFgYDVQQKDA9DZXJ0aWZpcm1hIFMuQS4xCzAJBgNVBAYTAkdUMB4X\n"
                + "DTEyMDcyNTE1MTA1MFoXDTE3MDcyNTE1MTA1MFowUzEhMB8GA1UEAwwYSW5mb3J1\n"
                + "bSBDb25zdWx0aW5nLCBTLkEuMSEwHwYDVQQKDBhJbmZvcnVtIENvbnN1bHRpbmcs\n"
                + "IFMuQS4xCzAJBgNVBAYTAkdUMIIBtjCCASsGByqGSM44BAEwggEeAoGBAMVac108\n"
                + "iKrhRlk5j8T+zbX6h5QqnJgeLae/wDNc4jlSF3gnBuXWB2WOo8F37BGPxWLYASGu\n"
                + "h6F3/nhUEOt/P9dlzCiT925219W75Hbp6Hm+8FbhaSH0dt1lsfJSXVr5fmo1wpGJ\n"
                + "vJlXwYfv6Wnc74wfl/FFjpFWlLNf9sVMxTSNAhUA2DbdlCGHX4G7QXAHJp933DxA\n"
                + "UOMCgYBMwUh3hmzH35xB53Smcw5rxt7/WLinuLhE/jcFEm60Wi0zNAQT250R1ovf\n"
                + "ApHSxdMjOY8KUcdenSRbFdJ4cFhTh2jNeV1562Ghv6nX0bAjNV9G9aDdMmDxdhpO\n"
                + "CyoW3qah+JEeNw9cBVIMBsYWRTKZRtlMEEceGvq4al8sdwho3AOBhAACgYAqS7cg\n"
                + "y9qyWwIagr18g8DVlV+sNq489MFg73Upq2G33dr/c4WJ+a6IlOKvdHNmZ42Q6GjZ\n"
                + "1E4zv/KvyIdiz2LJXjIwEh4UWZJKhCR5mytV/8LyLk6nh9h7hkC3Nt2ZjevBkmIC\n"
                + "3PKf/EufuX7Ttd/mBIbx7jCeK0GT0+OjWDVoLaNCMEAwHwYDVR0jBBgwFoAUz3d8\n"
                + "7/DWaDue0azWOBShI8c7qFAwHQYDVR0OBBYEFBp1EekJSHvln9kd5X9Gx+uOY+qL\n"
                + "MAkGByqGSM44BAMDMAAwLQIVAKcGMCXgeOdlT64SGAgykWBBc3N/AhQBovoNGxdS\n"
                + "zaIuCxUpsnnuEcByJA==\n"
                + "&lt;/ds:X509Certificate&gt;&lt;/ds:X509Data&gt;&lt;/ds:KeyInfo&gt;&lt;/FCAE&gt;&lt;/CAE&gt;\n"
                + "  &lt;/Documento&gt;&lt;Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"&gt;&lt;SignedInfo&gt;&lt;CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#dsa-sha1\" /&gt;&lt;Reference URI=\"#FACE44252\"&gt;&lt;Transforms&gt;&lt;Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;/Transforms&gt;&lt;DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /&gt;&lt;DigestValue&gt;XxGqRvYQLxmR3s8dsH/qjZy3XPg=&lt;/DigestValue&gt;&lt;/Reference&gt;&lt;/SignedInfo&gt;&lt;SignatureValue&gt;\n"
                + "o5a0pwaIrU5tZX/obCThs3ypQRbXnSuuOIuWU93nHceflCfz8LvFrg==\n"
                + "&lt;/SignatureValue&gt;&lt;KeyInfo&gt;&lt;KeyValue&gt;&lt;DSAKeyValue&gt;&lt;P&gt;\n"
                + "4SxI6zeTSifLH4Nt9tMy8NZKorvdm/mYQbcexxQTXj4gOYV4tgjVGTh0Pk0ZQisy\n"
                + "lb9UY7csvMkQs1gOq+CTqAVFDsEBYyR3HAD/Jc4l8Ks+ZkOJGPQFYmbS9b/Yza34\n"
                + "r1ijCE5UJVjkKxmpPKIUywAOkxEdnVf2PXpvvQ4ipkk=\n"
                + "&lt;/P&gt;&lt;Q&gt;\n"
                + "2xyECo9nSODJR3sU70DdmpiMAXk=\n"
                + "&lt;/Q&gt;&lt;G&gt;\n"
                + "bEmBnx8k80b/23S7f5ZcrJQgUUH25sGk4oA8H26I2bOR9mrJdK3p7evfwTfKH3Vq\n"
                + "0XcHI6Let9hQwumEiIW+PaJGTZnFF/2egMIjhg7Ljpf8y21wL8ImLhf11/rKQw5z\n"
                + "ZG4OoS5h/9zzJExsnm3JWyzzUIPhslSkpx4b1yyrMtk=\n"
                + "&lt;/G&gt;&lt;Y&gt;\n"
                + "NIKC6b6rUKSedGKTjxSb8Z0aI/r1+dulrz6nAjhuTDmznODgWO6dr957cuPCxly2\n"
                + "3bmjOzZit9LjUKZil0G+TnEo9ExEzJObAZZs3lunR+FZTff50el/PybD7fWJJfqm\n"
                + "2h63ochPR7grkwhEdaC+y8kMkqgAm+PH012c7gEuobE=\n"
                + "&lt;/Y&gt;&lt;/DSAKeyValue&gt;&lt;/KeyValue&gt;&lt;X509Data&gt;&lt;X509Certificate&gt;\n"
                + "MIIDSjCCAwgCCQD9guaTc8XJCDAJBgcqhkjOOAQDMIGJMQswCQYDVQQGEwJHVDES\n"
                + "MBAGA1UECAwJR1VBVEVNQUxBMRIwEAYDVQQHDAlHVUFURU1BTEExEzARBgNVBAoM\n"
                + "CkNPRElNSVNBwoAxETAPBgNVBAsMCENPRElNSVNBMREwDwYDVQQDDAhDT0RJTUlT\n"
                + "QTEXMBUGCSqGSIb3DQEJARYIQ09ESU1JU0EwHhcNMTIxMTE1MjA1MDE5WhcNMjIx\n"
                + "MTEzMjA1MDE5WjCBiTELMAkGA1UEBhMCR1QxEjAQBgNVBAgMCUdVQVRFTUFMQTES\n"
                + "MBAGA1UEBwwJR1VBVEVNQUxBMRMwEQYDVQQKDApDT0RJTUlTQcKAMREwDwYDVQQL\n"
                + "DAhDT0RJTUlTQTERMA8GA1UEAwwIQ09ESU1JU0ExFzAVBgkqhkiG9w0BCQEWCENP\n"
                + "RElNSVNBMIIBtjCCASsGByqGSM44BAEwggEeAoGBAOEsSOs3k0onyx+DbfbTMvDW\n"
                + "SqK73Zv5mEG3HscUE14+IDmFeLYI1Rk4dD5NGUIrMpW/VGO3LLzJELNYDqvgk6gF\n"
                + "RQ7BAWMkdxwA/yXOJfCrPmZDiRj0BWJm0vW/2M2t+K9YowhOVCVY5CsZqTyiFMsA\n"
                + "DpMRHZ1X9j16b70OIqZJAhUA2xyECo9nSODJR3sU70DdmpiMAXkCgYBsSYGfHyTz\n"
                + "Rv/bdLt/llyslCBRQfbmwaTigDwfbojZs5H2asl0rent69/BN8ofdWrRdwcjot63\n"
                + "2FDC6YSIhb49okZNmcUX/Z6AwiOGDsuOl/zLbXAvwiYuF/XX+spDDnNkbg6hLmH/\n"
                + "3PMkTGyebclbLPNQg+GyVKSnHhvXLKsy2QOBhAACgYA0goLpvqtQpJ50YpOPFJvx\n"
                + "nRoj+vX526WvPqcCOG5MObOc4OBY7p2v3nty48LGXLbduaM7NmK30uNQpmKXQb5O\n"
                + "cSj0TETMk5sBlmzeW6dH4VlN9/nR6X8/JsPt9Ykl+qbaHrehyE9HuCuTCER1oL7L\n"
                + "yQySqACb48fTXZzuAS6hsTAJBgcqhkjOOAQDAzEAMC4CFQChrZMWeAjdgzoa0QQ3\n"
                + "y2cyR5WqkwIVAIS86vCGSPyWrsyMlT5miv63R4vX\n"
                + "&lt;/X509Certificate&gt;&lt;/X509Data&gt;&lt;/KeyInfo&gt;&lt;/Signature&gt;\n"
                + "  &lt;Personalizados&gt;\n"
                + "    &lt;campoString name=\"_strartofdoc\"&gt;&amp;lt;/ INICIO &amp;gt;&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"NITGFACE\"&gt;43430775&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"rangoInicialAutorizado\"&gt;000001&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"rangoFinalAutorizado\"&gt;100000&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"fechaResolucion\"&gt;2013-03-22&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"TOTALENLETRAS\"&gt;CUATROCIENTOS VENTISIETE 00/100 GTQ&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"DISTRIBUCIONELECTRONICA\" /&gt;\n"
                + "    &lt;campoString name=\"TIPO\"&gt;FACE&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"NUMERORESOLUCION\"&gt;201356621072514&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"MONEDA\"&gt;GTQ&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"AÑO\"&gt;16&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"MES \"&gt;03&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"DIA\"&gt;15&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"ISR\"&gt;SUJETO A PAGOS TRIMESTRALES&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"RUTA\" /&gt;\n"
                + "    &lt;campoString name=\"IDVENDEDOR\"&gt;23&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"VENDEDOR\"&gt;23&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"TELEFONO\"&gt;5353-1535&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"PEDIDO\"&gt;118792&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"SERIEPEDIDO\"&gt;MO&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"PEDIDOPREVENTA\"&gt;56999&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"LEYENDA1\" /&gt;\n"
                + "    &lt;campoString name=\"ORDENCOMPRA\" /&gt;\n"
                + "    &lt;campoString name=\"CONDICIONP\" /&gt;\n"
                + "    &lt;campoString name=\"SERIE\"&gt;AA&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"RAZONSOCIAL\"&gt;ACADEMIA MODA Y ESTILO / LUCRECIA MEDINA&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"IDCLIENTE\"&gt;9560&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"TIPO\"&gt;FACE&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"APP_ORIGEN\"&gt;XDOC&lt;/campoString&gt;\n"
                + "    &lt;campoString name=\"PDF417\"&gt;777777770707070007773534721566410075621156636711640732300611335122507426302350673333077021377100500340726275300364332007620041651675311077277017405711320763712136404671107630061526431111075316767355620140762674374035111207753523244115474076520526643555440775117656376030407534545377720324072713344076300020736146617773102307262106102135100077753527413444540777777707000707007 \n"
                + "777777770707070007161273311160010076324201066077510766424541763177507237056420115460072620174236513220722461555630544207366131105423162076735624116711020751463212513337207736110340671331077221641311510610766706442700177107666314737077531075312772701117310762411315433545307766034441413175077225751670231400732152032671336407543661311172762077252173331066300777777707000707007 \n"
                + "777777770707070007777070035114447076453577273007620775601427217555107051174737642111072274614464715540706313763264122007773147251132552070240773321046210710631154627661307625041307711120074063132667047540751164632335724107203043235710044077701513007275720732272050237131207435736223104233076316404432045310736661254625333107654146537724462073730340315100210777777707000707007 \n"
                + "777777770707070007370300633351223076531411633746200726221273023171107560155763760371070443541222601310773654611355401607371235333050311073334713552501110766421544072055307246017763440033075511052077374570706113763440120007507010117376002076664215543505750760562032735102007177473676571155076453371273365740744637647073324007277120554066133077161022731176010777777707000707007 \n"
                + "777777770707070007261210066115520071772304531332710767364173461113107323151164034430074244543767511740726605503033502207504742366113150076737203060457220774143046333576407271644220545112074215426224101130763523004637560007561573721025400076225515617501310772607102761333007763573105102356076750754377306660776257150752011107326551564307100072630300463115620777777707000707007 \n"
                + "777777770707070007723525451754402072360461344311110732034733551072007760252360557310071322610711336210767535566155005707666124014035556073310160453312540722506643235550207053760314422333072365600045711420720736446736757507141020563335700072171440032653000705400257273113007721711007600655073226531365027300731363647737765307734310153375221077631614553544640777777707000707007 \n"
                + "777777770707070007764304114337577076226521023533240760111601532317507234024556375770073623315473025770726316200136111307135450777360320074415606762355660755671476337156407620525543357540076404747266411770732531104436411207602573214251000074072474413776660753432324115531007205326631217732077471167622433740722314244030155207542366302537710077065045127713110777777707000707007 \n"
                + "777777770707070007736714656215777072117176710045420767535045126455607637331241310376072312733037333060771421227013115507123176266343310072034010673752200704231533233061007322520304117110075327355507466330762456054773146607325311107224320076267712465237400773125401533107107772142153175411072610043006211130773064504573154307124533772400321073761564720177510777777707000707007 \n"
                + "777777770707070007017022723136000072733122441216400732756010457150207635710632202630071325213777265400772674610655117207556717736306455070673047222271300765715147364474007237176652264461072320435301151130766370007037562207702577345160111072335700554314420761447411576204207741402316333575076371224626543300777415644143313507021510766334660074134223611104600777777707000707007 \n"
                + "777777770707070007673462027005530075231432237516440736211720140231307623135002306245076653545033424400770460433307011107751215436313551073363576407475110742110773651551207772065151303451071551645007732030750700326225513307763501160117552076340205167111120770204637101051307560445633375325077414645267353300766771760610454607744146522215157076770460034015740777777707000707007 \n"
                + "777777770707070007607703277500446070014712337516000711555205663343007153305623770210076557176637147720727713044043212207735645323577105071113722120732340772630336117711207623510274334640076635310501664220733350310531120507011471547377102074410610637750200722305344411175607717102272155230076774057004255570777725427511644707473171263766001072453016771004420777777707000707007 \n"
                + "777777770707070007377202344711543071201543766115710776374227026713307727303241751200070733100653237600730075376540154607726712402343331076673550560164660717710366231105207762502340137570073241314432215720722702043202711107653555233604122076370120615500200767364353215733007710520333730762073353546720556610763155035720015707737754076413052071770021447333670777777707000707007 \n"
                + "777777770707070007213705722205512077261007355330210713544012777160107747045026235573077503501003735560746551562620545607110052613333612074351513226713210751456432277513707523063662055531073350556563755770777627171574723007732612513752230073740157572745320764562544626315707670043042375720077141313661075500720255005662317607237700227422035072117055222015520777777707000707007 \n"
                + "777777770707070007671364156335640076375146176011220713741657772664507622174016013140074427560126237300736270331153012007720726450277753073001517303226030766264650034513707204146412377522077134722151530110743350732266013307325530365170000073471167611540200722670107751460207323307061335000077667176050047570744711762250046507203150127704600076731061141334660777777707000707007 \n"
                + "777777770707070007664524440527553072010251237313240772351754216544007713136360330540072316010466115460763573433305130007636023644267553077370025663157670762077607623530007223460444325731076671537617764710751403013737703307004265732764013077725306700155770775704732713354507344045721237712073535522344130010764062133625651007461773632126600076607640403631310777777707000707007 \n"
                + "777777770707070007733040216315721074077235502137730722342750553310207460553622450013073675311465021040771314722335236007543701304377732076762047743751760775134326011145607632720235734662077225606741375500772031452601513007060232250251110074762360413176640753757007622370107203511363170110076711562576257440766344300042115707624333163320057077710060325135450777777707000707007 \n"
                + "777777770707070007146717476626751072116233064377320772064277054331107103137321402104076314542457304600767273122051757307075551367752104074615533231501560720420336310173107332651375610156072130562677122640727230206413750007570157333416500074172032262553000777302710771556507113344366633563072475226654735440761466106637130007776177563401152073267314764207750777777707000707007&lt;/campoString&gt;\n"
                + "    &lt;campoNumero name=\"TOTAL\"&gt;427.00&lt;/campoNumero&gt;\n"
                + "    &lt;campoNumero name=\"DESCUENTO\"&gt;0.00&lt;/campoNumero&gt;\n"
                + "    &lt;campoNumero name=\"NUMEROFAC\"&gt;44252&lt;/campoNumero&gt;\n"
                + "    &lt;campoNumero name=\"TASADECAMBIO\"&gt;7.6500000000&lt;/campoNumero&gt;\n"
                + "    &lt;campoNumero name=\"SUBTOTAL\"&gt;427.0&lt;/campoNumero&gt;\n"
                + "    &lt;campoNumero name=\"TOTALBONI\"&gt;0&lt;/campoNumero&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;1&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113019940&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;52.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;2&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113005851&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;104.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;3&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113005769&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;104.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;4&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113006599&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;52.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;5&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113005783&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;52.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;6&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113010985&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;104.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;7&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113005752&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;104.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;8&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113006605&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;52.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;52.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;9&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;C001&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;115.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;115.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;10&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113003055&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;combo&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;0.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;47.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "    &lt;CustDetalle&gt;\n"
                + "      &lt;CustDetNroLin&gt;11&lt;/CustDetNroLin&gt;\n"
                + "      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n"
                + "      &lt;CustDetAFN_02&gt;7702113003048&lt;/CustDetAFN_02&gt;\n"
                + "      &lt;CustDetAFN_03&gt;combo&lt;/CustDetAFN_03&gt;\n"
                + "      &lt;CustDetNUD_01&gt;0.00&lt;/CustDetNUD_01&gt;\n"
                + "      &lt;CustDetNUD_02&gt;0.00&lt;/CustDetNUD_02&gt;\n"
                + "      &lt;CustDetNUD_03&gt;47.00&lt;/CustDetNUD_03&gt;\n"
                + "    &lt;/CustDetalle&gt;\n"
                + "  &lt;/Personalizados&gt;\n"
                + "&lt;/DTE&gt;</TextData>\n"
                + "</ProcessResult>";
        return texto;

    }

}
