package testall;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Configuracion {

    private String empresas;
    private List<Empresa> empresa;

    public Configuracion(){
        empresa = new ArrayList();
    }
    
 
    public List<Empresa> getEmpresa() {
        return empresa;
    }

    @XmlElement
    public void setEmpresa(List<Empresa> listEmpresa) {
        this.empresa = listEmpresa;
    }

    public String getEmpresas() {
        return empresas;
    }

    @XmlElement
    public void setEmpresas(String empresas) {
        this.empresas = empresas;
    }

}
