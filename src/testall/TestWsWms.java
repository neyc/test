/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testall;

import com.mobilges.server.wms.comm.MgTransactionRC;
import com.mobilges.server.wms.comm.MgWmsTrDespacho;
import com.mobilges.server.wms.comm.MgWmsTrDespachoService;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;

/**
 *
 * @author nyucute
 */
public class TestWsWms {

    
    public static void main(String[] args) {

        TestWsWms test = new TestWsWms();
        
        List<Long> pListaIdt = new ArrayList();
        pListaIdt.add( Long.parseLong("2149") );
        pListaIdt.add( Long.parseLong("2150") );
        
        test.generarDespachos( pListaIdt , "192.168.0.119:8080", 1, "01");
        
        
    }
        

    public String generarDespachos(List<Long> pIdtPedidoPrograRuta, String pUrl, Integer pManejarSkuFijo, String pCodigoBodegaWms ) {
        String result = new String();
        try {
            MgWmsTrDespacho port = getPort( pUrl );
            List<MgTransactionRC> resultado = port.creaPedidoWms( pIdtPedidoPrograRuta, pManejarSkuFijo, pCodigoBodegaWms );
            for( MgTransactionRC outcome : resultado ){
                if( !outcome.isSuccessful() ){
                    return "fail";
                }
            }
            result = "ok";
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            result = "fail";
        }
        return result;
    }        
    public MgWmsTrDespacho getPort( String pUrl ) throws MalformedURLException{
        QName qname = new QName("http://comm.wms.server.mobilges.com/", "MgWmsTrDespachoService");
        String wsdl = "http://" + pUrl + "/MgWmsTrDespachoService/MgWmsTrDespacho?wsdl";
        MgWmsTrDespachoService service = new MgWmsTrDespachoService(new URL(wsdl), qname);
        MgWmsTrDespacho port = service.getMgWmsTrDespachoPort();
        return port;
    }
    
    
}
