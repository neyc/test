/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testall;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import org.tempuri.Core;
import org.tempuri.CoreSoap;

/**
 *
 * @author nyucute
 */
public class testWS {
    private QName qname = new QName("http://tempuri.org/", "Core");
        
    public static void main(String[] args) {
        testWS test = new testWS();
        test.testWS();
    }
    
    public void testWS(){
        
        String wsdl = "http://200.49.179.206/Signature.xDoc.Web.Services/core.asmx?wsdl";
        String archivoXMLRespuesta = new String();
        String referenciaArchivo = "@@file:C:\\Codimisa\\mobilges\\FACE_NPG_MOBILGES.txt";
        try {
            Core service = new Core( new URL(wsdl), qname );
            
            CoreSoap port = service.getCoreSoap();

            archivoXMLRespuesta = port.convertSignDocument( "CODIMIS", "administrador", "FACE", referenciaArchivo );
            System.out.println("XML:    "+archivoXMLRespuesta);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            archivoXMLRespuesta = "fail";
        }
    }    
        
        
}
