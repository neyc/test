/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testall;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nyucute
 */
@XmlRootElement(name = "Signature")
public class Signature {
    
    private String SignatureValue;

    public String getSignatureValue() {
        return SignatureValue;
    }

    @XmlElement
    public void setSignatureValue(String SignatureValue) {
        this.SignatureValue = SignatureValue;
    }

    
}
