package testall;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DTE")
public class Customer {


    private List<Signature> signatures;

    public List<Signature> getSignatures() {
        return signatures;
    }

    @XmlElement
    public void setSignatures(List<Signature> signatures) {
        this.signatures = signatures;
    }

    
}
