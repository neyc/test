package testall;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class JAXBExample {

    public static String ruta = "/Users/nyucute/Documents/testDocutec.xml";

    public static void main(String[] args) {

//        createXML();
//        readXML();
        readXMLJdom();

    }

    public static void createXML() {
        Customer customer = new Customer();
//HOLA
        try {
            File file = new File(ruta);
            JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(customer, file);
            jaxbMarshaller.marshal(customer, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static void readXML() {
        try {

            
            File file = new File(ruta);
            JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Customer customer = (Customer) jaxbUnmarshaller.unmarshal(file);
            System.out.println( customer.getSignatures() );

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
    
    public static void readXMLJdom(){
        
        try
        {
            SAXBuilder builder = new SAXBuilder();
            JAXBExample jax = new JAXBExample();
            String archivo = jax.archivo();
            InputStream stream = new ByteArrayInputStream( archivo.getBytes("UTF-8") ) ;            
            Document document = (Document) builder.build( stream );
            Element rootNode = document.getRootElement();
            List<Element> elementos = rootNode.getChildren();

            String esError = new String();
            String data = new String();
            String firmaElectronica = new String();

            for ( Element element : elementos ){
                if ( element.getName().equals("HasError") ){
                    esError = element.getValue();
                }else if ( element.getName().equals("TextData") ){
                    data = element.getValue();
                }
            }
            
            InputStream stream2 = new ByteArrayInputStream( data.getBytes("UTF-8") ) ;            
            Document document2 = (Document) builder.build( stream2 );
            Element rootNode2 = document2.getRootElement();
            List<Element> elementos2 = rootNode2.getChildren();
            List<Element> elementosSignature = new ArrayList();

            for ( Element element : elementos2 ){
                if ( element.getName().equals("Signature") )
                    elementosSignature = element.getChildren();
            }        

            for ( Element ele : elementosSignature ){
                if ( ele.getName().equals( "SignatureValue" ) )
                    firmaElectronica = ele.getValue() ;
            }

            
        }catch ( IOException io ) {
        }catch ( JDOMException jdomex ) {
        }
        }
    
    public String archivo(){
        return "<?xml version=\"1.0\"?>\n" +
"<ProcessResult>\n" +
"    <Process>\n" +
"        <Name>Signature.xDoc.Web.Services</Name>\n" +
"        <Type>6</Type>\n" +
"        <TypeDesc>appWebServices</TypeDesc>\n" +
"    </Process>\n" +
"    <HasError>false</HasError>\n" +
"    <Error>\n" +
"        <Code />\n" +
"        <Description />\n" +
"        <UIMessage />\n" +
"    </Error>\n" +
"    <ReturnValue>0</ReturnValue>\n" +
"    <TextData>&lt;?xml version=\"1.0\" encoding=\"utf-8\"?&gt;\n" +
"&lt;DTE xmlns:dte=\"http://se.sat.gob.gt/aplicaciones/DTE\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:gs1=\"urn:ean.ucc:pay:2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"&gt;\n" +
"  &lt;Documento Id=\"FACE44010\"&gt;\n" +
"    &lt;gs1:invoice creationDateTime=\"2016-03-08T00:00:00\" documentStatus=\"ORIGINAL\" xmlns:gs1=\"urn:ean.ucc:pay:2\"&gt;\n" +
"      &lt;contentVersion&gt;\n" +
"        &lt;versionIdentification&gt;1.0&lt;/versionIdentification&gt;\n" +
"      &lt;/contentVersion&gt;\n" +
"      &lt;documentStructureVersion&gt;\n" +
"        &lt;versionIdentification&gt;Version 1.0&lt;/versionIdentification&gt;\n" +
"      &lt;/documentStructureVersion&gt;\n" +
"      &lt;invoiceIdentification&gt;\n" +
"        &lt;uniqueCreatorIdentification&gt;FACE|201356621072514|FACE-66-AA-001|160000044010&lt;/uniqueCreatorIdentification&gt;\n" +
"        &lt;contentOwner&gt;\n" +
"          &lt;gln&gt;0000000000000&lt;/gln&gt;\n" +
"        &lt;/contentOwner&gt;\n" +
"      &lt;/invoiceIdentification&gt;\n" +
"      &lt;invoiceCurrency&gt;\n" +
"        &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n" +
"      &lt;/invoiceCurrency&gt;\n" +
"      &lt;invoiceType&gt;INVOICE&lt;/invoiceType&gt;\n" +
"      &lt;countryOfSupplyOfGoods&gt;\n" +
"        &lt;countryISOCode&gt;GT&lt;/countryISOCode&gt;\n" +
"      &lt;/countryOfSupplyOfGoods&gt;\n" +
"      &lt;buyer&gt;\n" +
"        &lt;partyIdentification&gt;\n" +
"          &lt;gln&gt;0000000000000&lt;/gln&gt;\n" +
"        &lt;/partyIdentification&gt;\n" +
"        &lt;nameAndAddress&gt;\n" +
"          &lt;city&gt;MIXCO&lt;/city&gt;\n" +
"          &lt;countryCode&gt;\n" +
"            &lt;countryISOCode&gt;0&lt;/countryISOCode&gt;\n" +
"          &lt;/countryCode&gt;\n" +
"          &lt;languageOfTheParty&gt;\n" +
"            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n" +
"          &lt;/languageOfTheParty&gt;\n" +
"          &lt;name&gt;SALON DE BELLEZA CONY&lt;/name&gt;\n" +
"          &lt;state&gt;GUATEMALA&lt;/state&gt;\n" +
"          &lt;streetAddressOne&gt;Manzana \"V\" lote 13 Villas Club El Dorado zona 8 Mixco, San Cristobal&lt;/streetAddressOne&gt;\n" +
"        &lt;/nameAndAddress&gt;\n" +
"        &lt;companyRegistrationNumber&gt;570988-1&lt;/companyRegistrationNumber&gt;\n" +
"      &lt;/buyer&gt;\n" +
"      &lt;seller&gt;\n" +
"        &lt;partyIdentification&gt;\n" +
"          &lt;gln&gt;0000000000000&lt;/gln&gt;\n" +
"          &lt;additionalPartyIdentification&gt;\n" +
"            &lt;additionalPartyIdentificationValue&gt;COMPANIA DISTRIBUIDORA MIREDA, S.A.&lt;/additionalPartyIdentificationValue&gt;\n" +
"            &lt;additionalPartyIdentificationType&gt;FOR_INTERNAL_USE_1&lt;/additionalPartyIdentificationType&gt;\n" +
"          &lt;/additionalPartyIdentification&gt;\n" +
"          &lt;additionalPartyIdentification&gt;\n" +
"            &lt;additionalPartyIdentificationValue&gt;9999&lt;/additionalPartyIdentificationValue&gt;\n" +
"            &lt;additionalPartyIdentificationType&gt;FOR_INTERNAL_USE_2&lt;/additionalPartyIdentificationType&gt;\n" +
"          &lt;/additionalPartyIdentification&gt;\n" +
"        &lt;/partyIdentification&gt;\n" +
"        &lt;nameAndAddress&gt;\n" +
"          &lt;city&gt;GUATEMALA&lt;/city&gt;\n" +
"          &lt;countryCode&gt;\n" +
"            &lt;countryISOCode&gt;GT&lt;/countryISOCode&gt;\n" +
"          &lt;/countryCode&gt;\n" +
"          &lt;languageOfTheParty&gt;\n" +
"            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n" +
"          &lt;/languageOfTheParty&gt;\n" +
"          &lt;name&gt;COMPANIA DISTRIBUIDORA MIREDA, S.A.&lt;/name&gt;\n" +
"          &lt;state&gt;GUATEMALA&lt;/state&gt;\n" +
"          &lt;streetAddressOne&gt;Avenida Ferrocarril 19-97 Zona 12 Empresarial El Cortijo int 610&lt;/streetAddressOne&gt;\n" +
"        &lt;/nameAndAddress&gt;\n" +
"        &lt;companyRegistrationNumber&gt;5835682&lt;/companyRegistrationNumber&gt;\n" +
"      &lt;/seller&gt;\n" +
"      &lt;invoiceLineItem number=\"1\"&gt;\n" +
"        &lt;invoicedQuantity&gt;\n" +
"          &lt;value&gt;24.00&lt;/value&gt;\n" +
"          &lt;unitOfMeasure&gt;\n" +
"            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n" +
"          &lt;/unitOfMeasure&gt;\n" +
"        &lt;/invoicedQuantity&gt;\n" +
"        &lt;transferOfOwnershipDate&gt;2016-03-08&lt;/transferOfOwnershipDate&gt;\n" +
"        &lt;amountInclusiveAllowancesCharges&gt;240.00&lt;/amountInclusiveAllowancesCharges&gt;\n" +
"        &lt;itemDescription&gt;\n" +
"          &lt;language&gt;\n" +
"            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n" +
"          &lt;/language&gt;\n" +
"          &lt;text&gt;KERATIN ULTRA ACTIVE TREATMENT 30 G&lt;/text&gt;\n" +
"        &lt;/itemDescription&gt;\n" +
"        &lt;itemPriceBaseQuantity&gt;\n" +
"          &lt;value&gt;8.93&lt;/value&gt;\n" +
"        &lt;/itemPriceBaseQuantity&gt;\n" +
"        &lt;itemPriceInclusiveAllowancesCharges&gt;10.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n" +
"        &lt;invoiceLineItemInformationAfterTaxes&gt;\n" +
"          &lt;amountExclusiveAllowancesCharges&gt;214.29&lt;/amountExclusiveAllowancesCharges&gt;\n" +
"        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n" +
"        &lt;invoiceLineTaxInformation&gt;\n" +
"          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n" +
"          &lt;taxableAmount&gt;214.29&lt;/taxableAmount&gt;\n" +
"          &lt;taxAmount&gt;25.71&lt;/taxAmount&gt;\n" +
"          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n" +
"        &lt;/invoiceLineTaxInformation&gt;\n" +
"      &lt;/invoiceLineItem&gt;\n" +
"      &lt;invoiceLineItem number=\"2\"&gt;\n" +
"        &lt;invoicedQuantity&gt;\n" +
"          &lt;value&gt;96.00&lt;/value&gt;\n" +
"          &lt;unitOfMeasure&gt;\n" +
"            &lt;measurementUnitCodeValue&gt;uni&lt;/measurementUnitCodeValue&gt;\n" +
"          &lt;/unitOfMeasure&gt;\n" +
"        &lt;/invoicedQuantity&gt;\n" +
"        &lt;transferOfOwnershipDate&gt;2016-03-08&lt;/transferOfOwnershipDate&gt;\n" +
"        &lt;amountInclusiveAllowancesCharges&gt;960.00&lt;/amountInclusiveAllowancesCharges&gt;\n" +
"        &lt;itemDescription&gt;\n" +
"          &lt;language&gt;\n" +
"            &lt;languageISOCode&gt;ES&lt;/languageISOCode&gt;\n" +
"          &lt;/language&gt;\n" +
"          &lt;text&gt;KERATIN ULTRA ACTIVE TREATMENT 30 G&lt;/text&gt;\n" +
"        &lt;/itemDescription&gt;\n" +
"        &lt;itemPriceBaseQuantity&gt;\n" +
"          &lt;value&gt;8.93&lt;/value&gt;\n" +
"        &lt;/itemPriceBaseQuantity&gt;\n" +
"        &lt;itemPriceInclusiveAllowancesCharges&gt;10.00&lt;/itemPriceInclusiveAllowancesCharges&gt;\n" +
"        &lt;invoiceLineItemInformationAfterTaxes&gt;\n" +
"          &lt;amountExclusiveAllowancesCharges&gt;857.14&lt;/amountExclusiveAllowancesCharges&gt;\n" +
"        &lt;/invoiceLineItemInformationAfterTaxes&gt;\n" +
"        &lt;invoiceLineTaxInformation&gt;\n" +
"          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n" +
"          &lt;taxableAmount&gt;857.14&lt;/taxableAmount&gt;\n" +
"          &lt;taxAmount&gt;102.86&lt;/taxAmount&gt;\n" +
"          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n" +
"        &lt;/invoiceLineTaxInformation&gt;\n" +
"      &lt;/invoiceLineItem&gt;\n" +
"      &lt;invoiceTotals&gt;\n" +
"        &lt;totalInvoiceAmount&gt;857.14&lt;/totalInvoiceAmount&gt;\n" +
"        &lt;totalAmountInvoiceAllowancesCharges&gt;0.00&lt;/totalAmountInvoiceAllowancesCharges&gt;\n" +
"        &lt;totalLineAmountExclusiveAllowancesCharges&gt;857.14&lt;/totalLineAmountExclusiveAllowancesCharges&gt;\n" +
"        &lt;totalTaxAmount&gt;102.86&lt;/totalTaxAmount&gt;\n" +
"        &lt;taxSubTotal&gt;\n" +
"          &lt;dutyTaxFeeType&gt;IVA&lt;/dutyTaxFeeType&gt;\n" +
"          &lt;taxableAmount&gt;857.14&lt;/taxableAmount&gt;\n" +
"          &lt;taxAmount&gt;102.86&lt;/taxAmount&gt;\n" +
"          &lt;taxPercentage&gt;12.00&lt;/taxPercentage&gt;\n" +
"        &lt;/taxSubTotal&gt;\n" +
"        &lt;totalInvoiceAmountPayable&gt;857.14&lt;/totalInvoiceAmountPayable&gt;\n" +
"        &lt;taxAccountingCurrency&gt;\n" +
"          &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n" +
"        &lt;/taxAccountingCurrency&gt;\n" +
"        &lt;baseAmount&gt;960.00&lt;/baseAmount&gt;\n" +
"      &lt;/invoiceTotals&gt;\n" +
"      &lt;taxCurrencyInformation&gt;\n" +
"        &lt;currencyConversionFrom&gt;\n" +
"          &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n" +
"        &lt;/currencyConversionFrom&gt;\n" +
"        &lt;currencyConversionTo&gt;\n" +
"          &lt;currencyISOCode&gt;GTQ&lt;/currencyISOCode&gt;\n" +
"        &lt;/currencyConversionTo&gt;\n" +
"        &lt;exchangeRate&gt;1.00&lt;/exchangeRate&gt;\n" +
"      &lt;/taxCurrencyInformation&gt;\n" +
"      &lt;extension&gt;\n" +
"        &lt;fechaResolucion&gt;2013-03-22&lt;/fechaResolucion&gt;\n" +
"        &lt;serieAutorizada&gt;FACE-66-AA-001&lt;/serieAutorizada&gt;\n" +
"        &lt;rangoInicialAutorizado&gt;000001&lt;/rangoInicialAutorizado&gt;\n" +
"        &lt;rangoFinalAutorizado&gt;100000&lt;/rangoFinalAutorizado&gt;\n" +
"        &lt;informacionRegimenIsr&gt;PAGO_TRIMESTRAL&lt;/informacionRegimenIsr&gt;\n" +
"      &lt;/extension&gt;\n" +
"    &lt;/gs1:invoice&gt;\n" +
"    &lt;CAE&gt;&lt;DCAE Id=\"CAE160000044010\"&gt;&lt;TipoCAE&gt;CAE&lt;/TipoCAE&gt;&lt;NITGFACE&gt;43430775&lt;/NITGFACE&gt;&lt;NITEmisor&gt;5835682&lt;/NITEmisor&gt;&lt;NITReceptor&gt;570988-1&lt;/NITReceptor&gt;&lt;Serie&gt;FACE-66-AA-001&lt;/Serie&gt;&lt;NumeroDocumento&gt;160000044010&lt;/NumeroDocumento&gt;&lt;FechaEmision&gt;2016-03-08&lt;/FechaEmision&gt;&lt;NumeroAutorizacion&gt;201356621072514&lt;/NumeroAutorizacion&gt;&lt;IngresosNetosGravados&gt;857.14&lt;/IngresosNetosGravados&gt;&lt;IVA&gt;102.86&lt;/IVA&gt;&lt;ImporteTotal&gt;960.00&lt;/ImporteTotal&gt;&lt;/DCAE&gt;&lt;FCAE&gt;&lt;ds:SignedInfo&gt;&lt;ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#dsa-sha1\" /&gt;&lt;ds:Reference URI=\"#CAE160000044010\"&gt;&lt;ds:Transforms&gt;&lt;ds:Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;/ds:Transforms&gt;&lt;ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /&gt;&lt;ds:DigestValue&gt;jxu/wyv+wh9aLpyE3W/X83jA87U=&lt;/ds:DigestValue&gt;&lt;/ds:Reference&gt;&lt;/ds:SignedInfo&gt;&lt;ds:SignatureValue&gt;\n" +
"Bk6y6luA0gaAjT/xxKKADUKKzTlgC7HO6RrwecMdIhr8OtQFX4dZuA==\n" +
"&lt;/ds:SignatureValue&gt;&lt;ds:KeyInfo&gt;&lt;ds:KeyValue&gt;&lt;ds:DSAKeyValue&gt;&lt;ds:P&gt;\n" +
"xVpzXTyIquFGWTmPxP7NtfqHlCqcmB4tp7/AM1ziOVIXeCcG5dYHZY6jwXfsEY/F\n" +
"YtgBIa6HoXf+eFQQ638/12XMKJP3bnbX1bvkdunoeb7wVuFpIfR23WWx8lJdWvl+\n" +
"ajXCkYm8mVfBh+/padzvjB+X8UWOkVaUs1/2xUzFNI0=\n" +
"&lt;/ds:P&gt;&lt;ds:Q&gt;\n" +
"2DbdlCGHX4G7QXAHJp933DxAUOM=\n" +
"&lt;/ds:Q&gt;&lt;ds:G&gt;\n" +
"TMFId4Zsx9+cQed0pnMOa8be/1i4p7i4RP43BRJutFotMzQEE9udEdaL3wKR0sXT\n" +
"IzmPClHHXp0kWxXSeHBYU4dozXldeethob+p19GwIzVfRvWg3TJg8XYaTgsqFt6m\n" +
"ofiRHjcPXAVSDAbGFkUymUbZTBBHHhr6uGpfLHcIaNw=\n" +
"&lt;/ds:G&gt;&lt;ds:Y&gt;\n" +
"Kku3IMvaslsCGoK9fIPA1ZVfrDauPPTBYO91Katht93a/3OFifmuiJTir3RzZmeN\n" +
"kOho2dROM7/yr8iHYs9iyV4yMBIeFFmSSoQkeZsrVf/C8i5Op4fYe4ZAtzbdmY3r\n" +
"wZJiAtzyn/xLn7l+07Xf5gSG8e4wnitBk9Pjo1g1aC0=\n" +
"&lt;/ds:Y&gt;&lt;/ds:DSAKeyValue&gt;&lt;/ds:KeyValue&gt;&lt;ds:X509Data&gt;&lt;ds:X509Certificate&gt;\n" +
"MIIDCTCCAsigAwIBAgIBATAJBgcqhkjOOAQDMEAxFzAVBgNVBAMMDkNlcnRpRmly\n" +
"bWFzIENBMRgwFgYDVQQKDA9DZXJ0aWZpcm1hIFMuQS4xCzAJBgNVBAYTAkdUMB4X\n" +
"DTEyMDcyNTE1MTA1MFoXDTE3MDcyNTE1MTA1MFowUzEhMB8GA1UEAwwYSW5mb3J1\n" +
"bSBDb25zdWx0aW5nLCBTLkEuMSEwHwYDVQQKDBhJbmZvcnVtIENvbnN1bHRpbmcs\n" +
"IFMuQS4xCzAJBgNVBAYTAkdUMIIBtjCCASsGByqGSM44BAEwggEeAoGBAMVac108\n" +
"iKrhRlk5j8T+zbX6h5QqnJgeLae/wDNc4jlSF3gnBuXWB2WOo8F37BGPxWLYASGu\n" +
"h6F3/nhUEOt/P9dlzCiT925219W75Hbp6Hm+8FbhaSH0dt1lsfJSXVr5fmo1wpGJ\n" +
"vJlXwYfv6Wnc74wfl/FFjpFWlLNf9sVMxTSNAhUA2DbdlCGHX4G7QXAHJp933DxA\n" +
"UOMCgYBMwUh3hmzH35xB53Smcw5rxt7/WLinuLhE/jcFEm60Wi0zNAQT250R1ovf\n" +
"ApHSxdMjOY8KUcdenSRbFdJ4cFhTh2jNeV1562Ghv6nX0bAjNV9G9aDdMmDxdhpO\n" +
"CyoW3qah+JEeNw9cBVIMBsYWRTKZRtlMEEceGvq4al8sdwho3AOBhAACgYAqS7cg\n" +
"y9qyWwIagr18g8DVlV+sNq489MFg73Upq2G33dr/c4WJ+a6IlOKvdHNmZ42Q6GjZ\n" +
"1E4zv/KvyIdiz2LJXjIwEh4UWZJKhCR5mytV/8LyLk6nh9h7hkC3Nt2ZjevBkmIC\n" +
"3PKf/EufuX7Ttd/mBIbx7jCeK0GT0+OjWDVoLaNCMEAwHwYDVR0jBBgwFoAUz3d8\n" +
"7/DWaDue0azWOBShI8c7qFAwHQYDVR0OBBYEFBp1EekJSHvln9kd5X9Gx+uOY+qL\n" +
"MAkGByqGSM44BAMDMAAwLQIVAKcGMCXgeOdlT64SGAgykWBBc3N/AhQBovoNGxdS\n" +
"zaIuCxUpsnnuEcByJA==\n" +
"&lt;/ds:X509Certificate&gt;&lt;/ds:X509Data&gt;&lt;/ds:KeyInfo&gt;&lt;/FCAE&gt;&lt;/CAE&gt;\n" +
"  &lt;/Documento&gt;&lt;Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"&gt;&lt;SignedInfo&gt;&lt;CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#dsa-sha1\" /&gt;&lt;Reference URI=\"#FACE44010\"&gt;&lt;Transforms&gt;&lt;Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /&gt;&lt;/Transforms&gt;&lt;DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /&gt;&lt;DigestValue&gt;fIHpuQz/3sFZYqAL4j66AittYQw=&lt;/DigestValue&gt;&lt;/Reference&gt;&lt;/SignedInfo&gt;&lt;SignatureValue&gt;\n" +
"WHMms8NUgUchjV0XezYjNirLsHBdeiBpxUlzqZHYyNjOsJs9EhStrw==\n" +
"&lt;/SignatureValue&gt;&lt;KeyInfo&gt;&lt;KeyValue&gt;&lt;DSAKeyValue&gt;&lt;P&gt;\n" +
"4SxI6zeTSifLH4Nt9tMy8NZKorvdm/mYQbcexxQTXj4gOYV4tgjVGTh0Pk0ZQisy\n" +
"lb9UY7csvMkQs1gOq+CTqAVFDsEBYyR3HAD/Jc4l8Ks+ZkOJGPQFYmbS9b/Yza34\n" +
"r1ijCE5UJVjkKxmpPKIUywAOkxEdnVf2PXpvvQ4ipkk=\n" +
"&lt;/P&gt;&lt;Q&gt;\n" +
"2xyECo9nSODJR3sU70DdmpiMAXk=\n" +
"&lt;/Q&gt;&lt;G&gt;\n" +
"bEmBnx8k80b/23S7f5ZcrJQgUUH25sGk4oA8H26I2bOR9mrJdK3p7evfwTfKH3Vq\n" +
"0XcHI6Let9hQwumEiIW+PaJGTZnFF/2egMIjhg7Ljpf8y21wL8ImLhf11/rKQw5z\n" +
"ZG4OoS5h/9zzJExsnm3JWyzzUIPhslSkpx4b1yyrMtk=\n" +
"&lt;/G&gt;&lt;Y&gt;\n" +
"NIKC6b6rUKSedGKTjxSb8Z0aI/r1+dulrz6nAjhuTDmznODgWO6dr957cuPCxly2\n" +
"3bmjOzZit9LjUKZil0G+TnEo9ExEzJObAZZs3lunR+FZTff50el/PybD7fWJJfqm\n" +
"2h63ochPR7grkwhEdaC+y8kMkqgAm+PH012c7gEuobE=\n" +
"&lt;/Y&gt;&lt;/DSAKeyValue&gt;&lt;/KeyValue&gt;&lt;X509Data&gt;&lt;X509Certificate&gt;\n" +
"MIIDSjCCAwgCCQD9guaTc8XJCDAJBgcqhkjOOAQDMIGJMQswCQYDVQQGEwJHVDES\n" +
"MBAGA1UECAwJR1VBVEVNQUxBMRIwEAYDVQQHDAlHVUFURU1BTEExEzARBgNVBAoM\n" +
"CkNPRElNSVNBwoAxETAPBgNVBAsMCENPRElNSVNBMREwDwYDVQQDDAhDT0RJTUlT\n" +
"QTEXMBUGCSqGSIb3DQEJARYIQ09ESU1JU0EwHhcNMTIxMTE1MjA1MDE5WhcNMjIx\n" +
"MTEzMjA1MDE5WjCBiTELMAkGA1UEBhMCR1QxEjAQBgNVBAgMCUdVQVRFTUFMQTES\n" +
"MBAGA1UEBwwJR1VBVEVNQUxBMRMwEQYDVQQKDApDT0RJTUlTQcKAMREwDwYDVQQL\n" +
"DAhDT0RJTUlTQTERMA8GA1UEAwwIQ09ESU1JU0ExFzAVBgkqhkiG9w0BCQEWCENP\n" +
"RElNSVNBMIIBtjCCASsGByqGSM44BAEwggEeAoGBAOEsSOs3k0onyx+DbfbTMvDW\n" +
"SqK73Zv5mEG3HscUE14+IDmFeLYI1Rk4dD5NGUIrMpW/VGO3LLzJELNYDqvgk6gF\n" +
"RQ7BAWMkdxwA/yXOJfCrPmZDiRj0BWJm0vW/2M2t+K9YowhOVCVY5CsZqTyiFMsA\n" +
"DpMRHZ1X9j16b70OIqZJAhUA2xyECo9nSODJR3sU70DdmpiMAXkCgYBsSYGfHyTz\n" +
"Rv/bdLt/llyslCBRQfbmwaTigDwfbojZs5H2asl0rent69/BN8ofdWrRdwcjot63\n" +
"2FDC6YSIhb49okZNmcUX/Z6AwiOGDsuOl/zLbXAvwiYuF/XX+spDDnNkbg6hLmH/\n" +
"3PMkTGyebclbLPNQg+GyVKSnHhvXLKsy2QOBhAACgYA0goLpvqtQpJ50YpOPFJvx\n" +
"nRoj+vX526WvPqcCOG5MObOc4OBY7p2v3nty48LGXLbduaM7NmK30uNQpmKXQb5O\n" +
"cSj0TETMk5sBlmzeW6dH4VlN9/nR6X8/JsPt9Ykl+qbaHrehyE9HuCuTCER1oL7L\n" +
"yQySqACb48fTXZzuAS6hsTAJBgcqhkjOOAQDAzEAMC4CFQChrZMWeAjdgzoa0QQ3\n" +
"y2cyR5WqkwIVAIS86vCGSPyWrsyMlT5miv63R4vX\n" +
"&lt;/X509Certificate&gt;&lt;/X509Data&gt;&lt;/KeyInfo&gt;&lt;/Signature&gt;\n" +
"  &lt;Personalizados&gt;\n" +
"    &lt;campoString name=\"_strartofdoc\"&gt;&amp;lt;/ INICIO &amp;gt;&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"NITGFACE\"&gt;43430775&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"rangoInicialAutorizado\"&gt;000001&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"rangoFinalAutorizado\"&gt;100000&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"fechaResolucion\"&gt;2013-03-22&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"TOTALENLETRAS\"&gt;NOVECIENTOS SESENTA&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"DISTRIBUCIONELECTRONICA\"&gt;cony.motta@hotmail.com&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"TIPO\"&gt;FACE&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"NUMERORESOLUCION\"&gt;201356621072514&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"MONEDA\"&gt;GTQ&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"AÑO\"&gt;16&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"MES \"&gt;03&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"DIA\"&gt;08&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"ISR\"&gt;SUJETO A PAGOS TRIMESTRALES&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"RUTA\"&gt;99&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"IDVENDEDOR\"&gt;23&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"VENDEDOR\"&gt;MIRIAM ARANA&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"TELEFONO\"&gt;5403-9783&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"PEDIDO\"&gt;118542&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"SERIEPEDIDO\"&gt;MO&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"PEDIDOPREVENTA\"&gt;56847&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"LEYENDA1\" /&gt;\n" +
"    &lt;campoString name=\"ORDENCOMPRA\" /&gt;\n" +
"    &lt;campoString name=\"CONDICIONP\"&gt;CREDITO&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"SERIE\"&gt;AA&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"RAZONSOCIAL\" /&gt;\n" +
"    &lt;campoString name=\"IDCLIENTE\"&gt;3890&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"TIPO\"&gt;FACE&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"APP_ORIGEN\"&gt;XDOC&lt;/campoString&gt;\n" +
"    &lt;campoString name=\"PDF417\"&gt;777777770707070007773534721566410075621156636711640732300611335122507426302350673333077021377100500340726275300364332007620041651675311077275035407513100743732136406673307410243726611331075316767355620140762674374035111207753523244115474076520526643555440775117656376030407534545377720324072713344076300020736146617773102307262106102135100077753527413444540777777707000707007 \n" +
"777777770707070007161273311160010076324201066077510766424541763177507237056420115460072620174236513220722461555630544207366131105423162076735624116711020751463212513337207736110340671331077221641311510610766706442700177107666314737077531075312772701117310762411315433545307766034441413175077225751670231400732152032671336407452661201173662077252173331066300777777707000707007 \n" +
"777777770707070007777070035114447077215174221444130762470510335130007332653134452355070152364425772220754153306326752007756015472711174076571063624447300771020563733127207712216636153111076061032237112660726631457701575307273312042140023077157274622775030734456133775402707743151262256611070632035404733330776217147204675107672640124711764073730340315100210777777707000707007 \n" +
"777777770707070007370300633351223077626607230175310742116376620777507723025111710074071457154673265330732702151115212007152317022363311070314632640401310747510466362176007741355122261463077763065735366100736211530001611207610640623611330072470454773564400770104031473755307700565155336003075407734210773220754235232271574007206511466205611077161022731176010777777707000707007 \n" +
"777777770707070007261210066115520077300767077335120742770462762211307723071571544210072207103722533300762134400502217107523662177513667076262110162135420765636460437732207617107320205510070277306442762110745725772627533107153232431337211076433316226344700762425122105710007333616411310760073110704453374520762742461031550007662720263035570072630300463115620777777707000707007 \n" +
"777777770707070007723525451754402072217540576213700777117457257105007137166732155521076061551676012000760200772014053107022371225123336073373252201712300777315236117661307260223563761311076757144162146440767713464014106407770544113516212077637715432464230735611104672357307620344100265157074215762235621150771301403373563107350116553775003077631614553544640777777707000707007 \n" +
"777777770707070007764304114337577076226521023533240760111601532317507234024556375770073623315473025770726316200136111307135450777360320074415606762355660755671476337156407620525543357540076404747266411770732531104436411207602573214251000074072474413776660753432324115531007605722671653332073071123662473300726310644074511207102362702133710077065045127713110777777707000707007 \n" +
"777777770707070007736714656215777072117176710045420767535045126455607637331241310376072312733037333060771421227013115507123176266343310072034010673752200704231533233061007322520304117110075327355507466330762456054773146607325311107224320076267712465237400773125401533107107772142153175411072610043006211130773064504573154307124533772400321073761564720177510777777707000707007 \n" +
"777777770707070007017022723136000072733122441216400732756010457150207635710632202630071325213777265400772674610655117207556717736306455070673047222271300765715147364474007237176652264461072320435301151130766370007037562207702577345160111072335700554314420761447411576204207741402316333575076371224626543300777415644143313507021510766334660074134223611104600777777707000707007 \n" +
"777777770707070007673462027005530075231432237516440736211720140231307623135002306245076653545033424400770460433307011107751215436313551073363576407475110742110773651551207772065151303451071551645007732030750700326225513307763501160117552076340205167111120770204637101051307560445633375325077414645267353300766771760610454607744146522215157076770460034015740777777707000707007 \n" +
"777777770707070007607703277500446070014712337516000711555205663343007153305623770210076557176637147720723713040003612207771601323537541071153322520732300772630336117711207623510274334640076635310501664220733350310531120507011471547377102074410610637750200722305344411175607717102272155230076774057004255570777725427511644707473171263766001072453016771004420777777707000707007 \n" +
"777777770707070007377202344711543071201543766115710776374227026713307727303241751200070733100653237600730075376540154607726712402343331076673550560164660717710366231105207762502340137570073241314432215720722702043202711107653555233604122076370120615500200767364353215733007710520333730762073353546720556610763155035720015707737754076413052071770021447333670777777707000707007 \n" +
"777777770707070007213705722205512077261007355330210713544012777160107747045026235573077503501003735560746551562620545607110052613333612074351513226713210751456432277513707523063662055531073350556563755770777627171574723007732612513752230073740157572745320764562544626315707670043042375720077141313661075500720255005662317607237700227422035072117055222015520777777707000707007 \n" +
"777777770707070007671364156335640076375364374011020713743677770446707620354234011340076607562304037100734250111373212207720504650077553073003715123224210764244412270117107602122636117720075330362375154710727716512240013307745334127334464073031563651104200722630547355420207763343025735444073623176410447130744315362654002107607510563300640076731061141334660777777707000707007 \n" +
"777777770707070007664524440527553072110351227313340763240655207555107613126370231540072207111477105570762572423205130007637032645376442076370034773046660762076616633530007333570544334630076770437707775700740502112637713207004375723774113076734207600155660765714622603245507245144620326713073425433255311100746353332615741107762753610205710076607640403631310777777707000707007 \n" +
"777777770707070007733040216315721074015231362771130700300532371314007602515446274475075417137223047600757154322315214407701521126355112074162623121773100737330300615123007760241532145530072775206650225100733061447315542107021372250310400070263774106473200757343147233775507253100762075000072311562463242010763700244012455307621722423660113077710060325135450777777707000707007 \n" +
"777777770707070007146717476626751073261032400137130720076433667117307622252334731556077504711236035510762621557147563007723316254605754077660547331372560770531366304453207067333046603663077262577202745530767346640376117307702166201417111076133153673106540767335676066012407631162126215402077055312224210150765734444363702007667264024305773073267314764207750777777707000707007&lt;/campoString&gt;\n" +
"    &lt;campoNumero name=\"TOTAL\"&gt;960.00&lt;/campoNumero&gt;\n" +
"    &lt;campoNumero name=\"DESCUENTO\"&gt;0.00&lt;/campoNumero&gt;\n" +
"    &lt;campoNumero name=\"NUMEROFAC\"&gt;44010&lt;/campoNumero&gt;\n" +
"    &lt;campoNumero name=\"TASADECAMBIO\"&gt;7.84387&lt;/campoNumero&gt;\n" +
"    &lt;campoNumero name=\"SUBTOTAL\"&gt;960.00&lt;/campoNumero&gt;\n" +
"    &lt;campoNumero name=\"TOTALBONI\"&gt;240.000000&lt;/campoNumero&gt;\n" +
"    &lt;CustDetalle&gt;\n" +
"      &lt;CustDetNroLin&gt;1&lt;/CustDetNroLin&gt;\n" +
"      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n" +
"      &lt;CustDetAFN_02&gt;7702113026122&lt;/CustDetAFN_02&gt;\n" +
"      &lt;CustDetAFN_03&gt;boni&lt;/CustDetAFN_03&gt;\n" +
"      &lt;CustDetNUD_01&gt;2.00&lt;/CustDetNUD_01&gt;\n" +
"      &lt;CustDetNUD_02&gt;240.00&lt;/CustDetNUD_02&gt;\n" +
"      &lt;CustDetNUD_03&gt;10.00&lt;/CustDetNUD_03&gt;\n" +
"    &lt;/CustDetalle&gt;\n" +
"    &lt;CustDetalle&gt;\n" +
"      &lt;CustDetNroLin&gt;2&lt;/CustDetNroLin&gt;\n" +
"      &lt;CustDetAFN_01&gt;BIEN&lt;/CustDetAFN_01&gt;\n" +
"      &lt;CustDetAFN_02&gt;7702113026122&lt;/CustDetAFN_02&gt;\n" +
"      &lt;CustDetAFN_03&gt;.&lt;/CustDetAFN_03&gt;\n" +
"      &lt;CustDetNUD_01&gt;1.00&lt;/CustDetNUD_01&gt;\n" +
"      &lt;CustDetNUD_02&gt;960.00&lt;/CustDetNUD_02&gt;\n" +
"      &lt;CustDetNUD_03&gt;10.00&lt;/CustDetNUD_03&gt;\n" +
"    &lt;/CustDetalle&gt;\n" +
"  &lt;/Personalizados&gt;\n" +
"&lt;/DTE&gt;</TextData>\n" +
"</ProcessResult>";
    } 
    
}
