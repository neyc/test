package testall;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nyucute
 */
@XmlRootElement
public class Empresa {

    private String empresaPadre;
    private String id;
    private String webservice;
    private String rutaformato;
    private String nombreformato;
    private Integer copias;

    public String getWebservice() {
        return webservice;
    }
    
    @XmlElement
    public void setWebservice(String webservice) {
        this.webservice = webservice;
    }

    public String getRutaformato() {
        return rutaformato;
    }

    @XmlElement
    public void setRutaformato(String rutaformato) {
        this.rutaformato = rutaformato;
    }

    public String getNombreformato() {
        return nombreformato;
    }

    @XmlElement
    public void setNombreformato(String nombreformato) {
        this.nombreformato = nombreformato;
    }

    public Integer getCopias() {
        return copias;
    }

    @XmlElement
    public void setCopias(Integer copias) {
        this.copias = copias;
    }


    public String getId() {
        return id;
    }

    @XmlAttribute
    public void setId(String id) {
        this.id = id;
    }

    public String getEmpresaPadre() {
        return empresaPadre;
    }

    @XmlAttribute
    public void setEmpresaPadre(String empresaPadre) {
        this.empresaPadre = empresaPadre;
    }
        
    
}
