
package com.mobilges.server.wms.comm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for creaPedidoWms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="creaPedidoWms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstIdtPedidoRuta" type="{http://www.w3.org/2001/XMLSchema}long" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="porSkuFijo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoBodegaWms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "creaPedidoWms", propOrder = {
    "lstIdtPedidoRuta",
    "porSkuFijo",
    "codigoBodegaWms"
})
public class CreaPedidoWms {

    @XmlElement(type = Long.class)
    protected List<Long> lstIdtPedidoRuta;
    protected int porSkuFijo;
    protected String codigoBodegaWms;

    /**
     * Gets the value of the lstIdtPedidoRuta property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lstIdtPedidoRuta property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLstIdtPedidoRuta().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getLstIdtPedidoRuta() {
        if (lstIdtPedidoRuta == null) {
            lstIdtPedidoRuta = new ArrayList<Long>();
        }
        return this.lstIdtPedidoRuta;
    }

    /**
     * Gets the value of the porSkuFijo property.
     * 
     */
    public int getPorSkuFijo() {
        return porSkuFijo;
    }

    /**
     * Sets the value of the porSkuFijo property.
     * 
     */
    public void setPorSkuFijo(int value) {
        this.porSkuFijo = value;
    }

    /**
     * Gets the value of the codigoBodegaWms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBodegaWms() {
        return codigoBodegaWms;
    }

    /**
     * Sets the value of the codigoBodegaWms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBodegaWms(String value) {
        this.codigoBodegaWms = value;
    }

}
