
package com.mobilges.server.wms.comm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anularPedidoDespachoWmsTest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="anularPedidoDespachoWmsTest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idtPedidoRuta" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "anularPedidoDespachoWmsTest", propOrder = {
    "idtPedidoRuta"
})
public class AnularPedidoDespachoWmsTest {

    protected Long idtPedidoRuta;

    /**
     * Gets the value of the idtPedidoRuta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdtPedidoRuta() {
        return idtPedidoRuta;
    }

    /**
     * Sets the value of the idtPedidoRuta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdtPedidoRuta(Long value) {
        this.idtPedidoRuta = value;
    }

}
