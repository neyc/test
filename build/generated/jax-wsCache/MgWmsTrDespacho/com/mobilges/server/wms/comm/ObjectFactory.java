
package com.mobilges.server.wms.comm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mobilges.server.wms.comm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GenerarPedidoDespachoWmsTest_QNAME = new QName("http://comm.wms.server.mobilges.com/", "generarPedidoDespachoWmsTest");
    private final static QName _AnularPedidoDespachoWmsTest_QNAME = new QName("http://comm.wms.server.mobilges.com/", "anularPedidoDespachoWmsTest");
    private final static QName _AnularPedidoDespachoWmsTestResponse_QNAME = new QName("http://comm.wms.server.mobilges.com/", "anularPedidoDespachoWmsTestResponse");
    private final static QName _AnularPedidoDespachoWmsResponse_QNAME = new QName("http://comm.wms.server.mobilges.com/", "anularPedidoDespachoWmsResponse");
    private final static QName _ActualizaNotaCargaDespachoWms_QNAME = new QName("http://comm.wms.server.mobilges.com/", "actualizaNotaCargaDespachoWms");
    private final static QName _CreaPedidoWmsResponse_QNAME = new QName("http://comm.wms.server.mobilges.com/", "creaPedidoWmsResponse");
    private final static QName _GenerarDespachoPorFacturaResponse_QNAME = new QName("http://comm.wms.server.mobilges.com/", "generarDespachoPorFacturaResponse");
    private final static QName _ActualizaNotaCargaDespachoWmsResponse_QNAME = new QName("http://comm.wms.server.mobilges.com/", "actualizaNotaCargaDespachoWmsResponse");
    private final static QName _GenerarDespachoPorFactura_QNAME = new QName("http://comm.wms.server.mobilges.com/", "generarDespachoPorFactura");
    private final static QName _AnularPedidoDespachoWms_QNAME = new QName("http://comm.wms.server.mobilges.com/", "anularPedidoDespachoWms");
    private final static QName _CreaPedidoWms_QNAME = new QName("http://comm.wms.server.mobilges.com/", "creaPedidoWms");
    private final static QName _GenerarPedidoDespachoWmsTestResponse_QNAME = new QName("http://comm.wms.server.mobilges.com/", "generarPedidoDespachoWmsTestResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mobilges.server.wms.comm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActualizaNotaCargaDespachoWmsResponse }
     * 
     */
    public ActualizaNotaCargaDespachoWmsResponse createActualizaNotaCargaDespachoWmsResponse() {
        return new ActualizaNotaCargaDespachoWmsResponse();
    }

    /**
     * Create an instance of {@link GenerarDespachoPorFactura }
     * 
     */
    public GenerarDespachoPorFactura createGenerarDespachoPorFactura() {
        return new GenerarDespachoPorFactura();
    }

    /**
     * Create an instance of {@link AnularPedidoDespachoWms }
     * 
     */
    public AnularPedidoDespachoWms createAnularPedidoDespachoWms() {
        return new AnularPedidoDespachoWms();
    }

    /**
     * Create an instance of {@link CreaPedidoWms }
     * 
     */
    public CreaPedidoWms createCreaPedidoWms() {
        return new CreaPedidoWms();
    }

    /**
     * Create an instance of {@link GenerarPedidoDespachoWmsTestResponse }
     * 
     */
    public GenerarPedidoDespachoWmsTestResponse createGenerarPedidoDespachoWmsTestResponse() {
        return new GenerarPedidoDespachoWmsTestResponse();
    }

    /**
     * Create an instance of {@link AnularPedidoDespachoWmsTestResponse }
     * 
     */
    public AnularPedidoDespachoWmsTestResponse createAnularPedidoDespachoWmsTestResponse() {
        return new AnularPedidoDespachoWmsTestResponse();
    }

    /**
     * Create an instance of {@link AnularPedidoDespachoWmsResponse }
     * 
     */
    public AnularPedidoDespachoWmsResponse createAnularPedidoDespachoWmsResponse() {
        return new AnularPedidoDespachoWmsResponse();
    }

    /**
     * Create an instance of {@link ActualizaNotaCargaDespachoWms }
     * 
     */
    public ActualizaNotaCargaDespachoWms createActualizaNotaCargaDespachoWms() {
        return new ActualizaNotaCargaDespachoWms();
    }

    /**
     * Create an instance of {@link CreaPedidoWmsResponse }
     * 
     */
    public CreaPedidoWmsResponse createCreaPedidoWmsResponse() {
        return new CreaPedidoWmsResponse();
    }

    /**
     * Create an instance of {@link GenerarDespachoPorFacturaResponse }
     * 
     */
    public GenerarDespachoPorFacturaResponse createGenerarDespachoPorFacturaResponse() {
        return new GenerarDespachoPorFacturaResponse();
    }

    /**
     * Create an instance of {@link AnularPedidoDespachoWmsTest }
     * 
     */
    public AnularPedidoDespachoWmsTest createAnularPedidoDespachoWmsTest() {
        return new AnularPedidoDespachoWmsTest();
    }

    /**
     * Create an instance of {@link GenerarPedidoDespachoWmsTest }
     * 
     */
    public GenerarPedidoDespachoWmsTest createGenerarPedidoDespachoWmsTest() {
        return new GenerarPedidoDespachoWmsTest();
    }

    /**
     * Create an instance of {@link MgTransactionRC }
     * 
     */
    public MgTransactionRC createMgTransactionRC() {
        return new MgTransactionRC();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarPedidoDespachoWmsTest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "generarPedidoDespachoWmsTest")
    public JAXBElement<GenerarPedidoDespachoWmsTest> createGenerarPedidoDespachoWmsTest(GenerarPedidoDespachoWmsTest value) {
        return new JAXBElement<GenerarPedidoDespachoWmsTest>(_GenerarPedidoDespachoWmsTest_QNAME, GenerarPedidoDespachoWmsTest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularPedidoDespachoWmsTest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "anularPedidoDespachoWmsTest")
    public JAXBElement<AnularPedidoDespachoWmsTest> createAnularPedidoDespachoWmsTest(AnularPedidoDespachoWmsTest value) {
        return new JAXBElement<AnularPedidoDespachoWmsTest>(_AnularPedidoDespachoWmsTest_QNAME, AnularPedidoDespachoWmsTest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularPedidoDespachoWmsTestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "anularPedidoDespachoWmsTestResponse")
    public JAXBElement<AnularPedidoDespachoWmsTestResponse> createAnularPedidoDespachoWmsTestResponse(AnularPedidoDespachoWmsTestResponse value) {
        return new JAXBElement<AnularPedidoDespachoWmsTestResponse>(_AnularPedidoDespachoWmsTestResponse_QNAME, AnularPedidoDespachoWmsTestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularPedidoDespachoWmsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "anularPedidoDespachoWmsResponse")
    public JAXBElement<AnularPedidoDespachoWmsResponse> createAnularPedidoDespachoWmsResponse(AnularPedidoDespachoWmsResponse value) {
        return new JAXBElement<AnularPedidoDespachoWmsResponse>(_AnularPedidoDespachoWmsResponse_QNAME, AnularPedidoDespachoWmsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizaNotaCargaDespachoWms }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "actualizaNotaCargaDespachoWms")
    public JAXBElement<ActualizaNotaCargaDespachoWms> createActualizaNotaCargaDespachoWms(ActualizaNotaCargaDespachoWms value) {
        return new JAXBElement<ActualizaNotaCargaDespachoWms>(_ActualizaNotaCargaDespachoWms_QNAME, ActualizaNotaCargaDespachoWms.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreaPedidoWmsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "creaPedidoWmsResponse")
    public JAXBElement<CreaPedidoWmsResponse> createCreaPedidoWmsResponse(CreaPedidoWmsResponse value) {
        return new JAXBElement<CreaPedidoWmsResponse>(_CreaPedidoWmsResponse_QNAME, CreaPedidoWmsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarDespachoPorFacturaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "generarDespachoPorFacturaResponse")
    public JAXBElement<GenerarDespachoPorFacturaResponse> createGenerarDespachoPorFacturaResponse(GenerarDespachoPorFacturaResponse value) {
        return new JAXBElement<GenerarDespachoPorFacturaResponse>(_GenerarDespachoPorFacturaResponse_QNAME, GenerarDespachoPorFacturaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizaNotaCargaDespachoWmsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "actualizaNotaCargaDespachoWmsResponse")
    public JAXBElement<ActualizaNotaCargaDespachoWmsResponse> createActualizaNotaCargaDespachoWmsResponse(ActualizaNotaCargaDespachoWmsResponse value) {
        return new JAXBElement<ActualizaNotaCargaDespachoWmsResponse>(_ActualizaNotaCargaDespachoWmsResponse_QNAME, ActualizaNotaCargaDespachoWmsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarDespachoPorFactura }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "generarDespachoPorFactura")
    public JAXBElement<GenerarDespachoPorFactura> createGenerarDespachoPorFactura(GenerarDespachoPorFactura value) {
        return new JAXBElement<GenerarDespachoPorFactura>(_GenerarDespachoPorFactura_QNAME, GenerarDespachoPorFactura.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularPedidoDespachoWms }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "anularPedidoDespachoWms")
    public JAXBElement<AnularPedidoDespachoWms> createAnularPedidoDespachoWms(AnularPedidoDespachoWms value) {
        return new JAXBElement<AnularPedidoDespachoWms>(_AnularPedidoDespachoWms_QNAME, AnularPedidoDespachoWms.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreaPedidoWms }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "creaPedidoWms")
    public JAXBElement<CreaPedidoWms> createCreaPedidoWms(CreaPedidoWms value) {
        return new JAXBElement<CreaPedidoWms>(_CreaPedidoWms_QNAME, CreaPedidoWms.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarPedidoDespachoWmsTestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://comm.wms.server.mobilges.com/", name = "generarPedidoDespachoWmsTestResponse")
    public JAXBElement<GenerarPedidoDespachoWmsTestResponse> createGenerarPedidoDespachoWmsTestResponse(GenerarPedidoDespachoWmsTestResponse value) {
        return new JAXBElement<GenerarPedidoDespachoWmsTestResponse>(_GenerarPedidoDespachoWmsTestResponse_QNAME, GenerarPedidoDespachoWmsTestResponse.class, null, value);
    }

}
