
package com.mobilges.server.wms.comm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for generarPedidoDespachoWmsTest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generarPedidoDespachoWmsTest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idtPedidoRuta" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="porSkuFijo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoBodegaWms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generarPedidoDespachoWmsTest", propOrder = {
    "idtPedidoRuta",
    "porSkuFijo",
    "codigoBodegaWms"
})
public class GenerarPedidoDespachoWmsTest {

    protected Long idtPedidoRuta;
    protected int porSkuFijo;
    protected String codigoBodegaWms;

    /**
     * Gets the value of the idtPedidoRuta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdtPedidoRuta() {
        return idtPedidoRuta;
    }

    /**
     * Sets the value of the idtPedidoRuta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdtPedidoRuta(Long value) {
        this.idtPedidoRuta = value;
    }

    /**
     * Gets the value of the porSkuFijo property.
     * 
     */
    public int getPorSkuFijo() {
        return porSkuFijo;
    }

    /**
     * Sets the value of the porSkuFijo property.
     * 
     */
    public void setPorSkuFijo(int value) {
        this.porSkuFijo = value;
    }

    /**
     * Gets the value of the codigoBodegaWms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBodegaWms() {
        return codigoBodegaWms;
    }

    /**
     * Sets the value of the codigoBodegaWms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBodegaWms(String value) {
        this.codigoBodegaWms = value;
    }

}
